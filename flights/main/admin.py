from django.contrib import admin

from .models import Leg, Segment, Variant

@admin.register(Leg)
class LegAdmin(admin.ModelAdmin):
    pass


@admin.register(Segment)
class SegmentAdmin(admin.ModelAdmin):
    pass


@admin.register(Variant)
class VariantAdmin(admin.ModelAdmin):
    pass
