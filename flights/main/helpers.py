import datetime


def el2dict(element):
    """
    Преобразовует LXML Element в словарь

    Ключом выступает тег элемента, а значением текст. Если в XML несколько
    повторяющихся тегов, то их значения будут представлены массивом.
    """
    result = {}
    for node in element:
        if len(node):  # есть потомки
            value = el2dict(node)
        else:
            value = node.text

        # <image>, <image> => 'image': [val1, val2, ...]
        key = node.tag
        if key in result and type(result[key]) != list:
            result[key] = [result[key]]
            result[key].append(value)
        else:
            result[key] = value

    return result


def yyyymmdd_to_date(date_str):
    return datetime.datetime.strptime(date_str, '%Y-%m-%d').date()


def hhmm_to_time(time_str):
    return datetime.datetime.strptime(time_str, '%H:%M').time()


def ensure_list(object):
    if not isinstance(object, list):
        return [object]
    return object
