import datetime
import logging

from lxml import etree
from django.db import transaction

from .helpers import el2dict, yyyymmdd_to_date, hhmm_to_time, ensure_list
from .models import Leg, Segment, Variant


log = logging.getLogger(__name__)

class Importer:

    @staticmethod
    def parse(file_path):
        # намеренно выдадим громкую ошибку, если файл не существует, не читается и т. д.
        feed = etree.parse(file_path)
        sync = Syncronizer()

        for index, variant_xml in enumerate(feed.iter('variant')):
            if index <= 1982:
                log.debug('[%d] importing variant', index)

                # сначала преобразуем XML в питоновые объекты на основе нативных
                # типов данных - словари, числа, DateTime
                variant = Deserializer.deserialize_variant(variant_xml)

                # потом сохраним вариант в базе
                sync.persist(variant)


class Deserializer:

    @staticmethod
    def deserialize_variant(xml_element):
        """
        Преобразует представление варианта из XML в структуру нативных данных,
        которую потом фабрика преобразует в набор моделей, пригодных к сохранению
        в базе данных
        """

        data = el2dict(xml_element)

        Deserializer.prepare_variant(data)
        Deserializer.prepare_segments(data)

        for s in data['segment']:
            Deserializer.prepare_flights(s)

        # for k,v in data.items(): print(k, v)
        return data

    @staticmethod
    def prepare_variant(data):
        data['price'] = int(data['price'])
        data['isCharter'] = True if data['isCharter'] == 'true' else False

    @staticmethod
    def prepare_segments(variant):
        # сегменты - всегда словарь
        variant['segment'] = ensure_list(variant['segment'])

    @staticmethod
    def prepare_flights(segment):
        # конвертация типов для полета
        segment['flight'] = ensure_list(segment['flight'])

        for flight in segment['flight']:
            flight['cabin'] = True if flight['cabin'] == 'Y' else False
            flight['departureDate'] = yyyymmdd_to_date(flight['departureDate'])
            flight['departureTime'] = hhmm_to_time(flight['departureTime'])
            flight['arrivalDate'] = yyyymmdd_to_date(flight['arrivalDate'])
            flight['arrivalTime'] = hhmm_to_time(flight['arrivalTime'])


class Syncronizer:
    """
    Готовит на основе десериализованных данных связанный набор моделей
    для одного варианта и сохраняет его в базу данных одной атомарной
    операцией

    Этот класс состоит из билд-методов, которые строят инстансы по данным
    из десериализованного xml. Билд-методы считают агрегатные данные
    в процессе построения модели. Так, например, для сегмента рассчитывается
    время в пути.

    Также этот класс определяет, если модели уже есть в базе и обновляет их
    (или пропускает).
    """

    def persist(self, data):
        with transaction.atomic():
            self.save_variant(data)

    def save_variant(self, variant_data):
        """
        Сохраняет вариант, предварительно создав все зависимые модели
        """
        segments = []
        for segment_data in variant_data['segment']:
            segment = self.save_segment(segment_data)
            segments.append(segment)

        # код - айдишник этого варианта, по нему ищем в базе
        code = gen_variant_code(segments)

        try:
            existing = Variant.objects.get(code=code)
        except Variant.DoesNotExist:
            existing = None

        if existing:
            log.debug('found existing variant for code %s with id %d, skipping',
                      code, existing.id)
            updated = self.update_variant(existing, variant_data)
            return updated

        log.debug('creating new variant')
        variant = self.build_variant(variant_data, segments, code)
        variant.save()
        variant.segments.set(segments)

        return variant

    def build_variant(self, variant_data, segments, code):
        """
        Строит инстанс варианта по десериализованным данным и сохраненным
        моделям сегментов

        Вычисляет также общее время вылета, прилета, в пути для всего
        варианта целиком, по данным из сегментов.
        """
        variant = Variant()
        variant.code = code

        first_segment = segments[0]
        last_segment = segments[-1]

        # первый сегмент
        variant.departure = first_segment.departure
        variant.departure_date = first_segment.departure_date
        variant.departure_time = first_segment.departure_time

        # последней
        variant.arrival = last_segment.arrival
        variant.arrival_date = last_segment.arrival_date
        variant.arrival_time = last_segment.arrival_time

        # время в пути
        delta = last_segment.departure_datetime - first_segment.departure_datetime
        variant.total_seconds = delta.total_seconds()

        return variant

    def update_variant(self, obj, data):
        # todo: обновление варианта данными из data
        return obj

    def save_segment(self, segment_data):
        """
        Сохраняет сегмент в базе данных и возвращает модель
        """
        legs = []
        for leg_data in segment_data['flight']:
            leg = self.save_leg(leg_data)
            legs.append(leg)

        code = gen_segment_code(legs)

        try:
            existing_segment = Segment.objects.get(code=code)
        except Segment.DoesNotExist:
            existing_segment = None

        if existing_segment:
            log.debug('found existing segment for code %s with id %d, skipping',
                      code, existing_segment.id)
            updated = self.update_segment(existing_segment, segment_data)
            return updated

        log.debug('creating new segment')
        segment = self.build_segment(segment_data, legs, code)
        segment.save()
        log.debug('binding legs')
        segment.legs.set(legs)  # m2m после save должен идти
        return segment

    def build_segment(self, segment_data, legs, code):
        """
        Строит сегмент и сохраняет агрегатные данные из legs

        Этот метод не сохраняет m2m связи в segment.legs
        """
        segment = Segment()
        segment.code = code

        first_departure = legs[0]
        last_arrival = legs[-1]

        # вылет - первый
        segment.departure = first_departure.departure
        segment.departure_date = first_departure.departure_date
        segment.departure_time = first_departure.departure_time

        # прибытие - последней из перелетов
        segment.arrival = last_arrival.arrival
        segment.arrival_date = last_arrival.arrival_date
        segment.arrival_time = last_arrival.arrival_time

        # время в пути
        delta = last_arrival.departure_datetime - first_departure.departure_datetime
        segment.total_seconds = delta.total_seconds()

        return segment

    def update_segment(self, obj, data):
        return obj

    def save_leg(self, leg_data):
        """
        Сохраняет leg в базе данных и возвращает модель
        """
        code = gen_leg_code(leg_data)  # код - хеш от данных перелета

        try:
            existing_leg = Leg.objects.get(code=code)
        except Leg.DoesNotExist:
            existing_leg = None

        if existing_leg:
            # такой отрезок уже есть в базе
            log.debug('found existing leg for code %s with id %d, updating', code, existing_leg.id)
            # (тут можно вызвать код обновления)
            updated = self.update_leg(existing_leg, leg_data)
            return updated
        else:
            # отрезка нет в базе, создадим
            log.debug('creating new leg for code %s', code)
            leg = self.build_leg(leg_data, code)
            leg.save()
            return leg

    def build_leg(self, data, code):
        """
        Строит экземпляр Leg по данным из data
        """
        leg = Leg()
        leg.code = code

        FIELD_MAP = {
            'operating_carrier': 'operatingCarrier',
            'marketing_carrier': 'marketingCarrier',
            'number': 'number',
            'departure': 'departure',
            'departure_date': 'departureDate',
            'departure_time': 'departureTime',
            'arrival': 'arrival',
            'arrival_date': 'arrivalDate',
            'arrival_time': 'arrivalTime',
            'equipment': 'equipment',
            'cabin': 'cabin',
            'baggage': 'baggage',
            'fare_code': 'fareCode',
        }

        # скопируем значения
        for attr_name, data_key in FIELD_MAP.items():
            # мы рассчитываем, что все поля есть в исходном xml
            value = data[data_key]
            setattr(leg, attr_name, value)

        return leg

    def update_leg(self, obj, data):
        # todo: update leg code
        return obj


def gen_leg_code(leg_data):
    """
    Генерирует хеш для перелета, по которому его можно найти в базе
    """
    assert isinstance(leg_data['departureDate'], datetime.date)
    assert isinstance(leg_data['departureTime'], datetime.time)
    assert isinstance(leg_data['arrivalDate'], datetime.date)
    assert isinstance(leg_data['arrivalTime'], datetime.time)

    return ''.join([
        leg_data['operatingCarrier'],
        str(leg_data['number']),
        leg_data['departure'],
        '{:%Y%m%d}'.format(leg_data['departureDate']),
        '{:%H%M}'.format(leg_data['departureTime']),
        leg_data['arrival'],
        '{:%Y%m%d}'.format(leg_data['arrivalDate']),
        '{:%H%M}'.format(leg_data['arrivalTime']),
    ])


def gen_segment_code(legs):
    """
    Код сегмента зависит исключительно от перелетов, которые в него входят
    """
    return '-'.join([str(l.id) for l in legs])


def gen_variant_code(segments):
    return '-'.join([str(s.id) for s in segments])
