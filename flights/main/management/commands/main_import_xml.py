import logging

from django.core.management.base import BaseCommand, CommandError

from flights.main.importer import Importer

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Импортирует перелеты из XML-файла'

    def add_arguments(self, parser):
        # positional arguments
        parser.add_argument('file_path', type=str)

    def handle(self, *args, **options):
        log.info('Starting main_import_xml')
        Importer.parse(options['file_path'])
        log.info('main_import_xml is done')
