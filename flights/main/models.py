"""
Сегмент - это последовательные перелеты с пересадкой. Если я лечу в Хельсинки
с пересадкой в Риге - это сегмент. А послезавтра лечу назад - это другой.
"""

import datetime

from django.db import models

from . import helpers


class DateTimeMixin:
    @property
    def departure_datetime(self):
        return datetime.datetime.combine(self.departure_date, self.departure_time)

    @property
    def arrival_datetime(self):
        return datetime.datetime.combine(self.arrival_date, self.arrival_time)


class Leg(models.Model, DateTimeMixin):
    """
    Один перелет из сложного маршрута Segment
    """

    code = models.CharField('Внутренний код', max_length=100, unique=True)

    operating_carrier = models.CharField('Перевозчик', max_length=50)
    marketing_carrier = models.CharField('Перевозчик (продавец)', max_length=50)
    number = models.IntegerField('Номер рейса')

    departure = models.CharField('Отправление', max_length=3)
    departure_date = models.DateField('Дата отправления')
    departure_time = models.TimeField('Время отправления')

    arrival = models.CharField('Прибытие', max_length=3)
    arrival_date = models.DateField('Дата прибытия')
    arrival_time = models.TimeField('Время прибытия')

    equipment = models.CharField('Модель самолета', max_length=50)
    cabin = models.BooleanField('Эконом-класс')
    baggage = models.CharField('Багаж', max_length=50)
    fare_code = models.CharField('Код тарифа', max_length=50)

    class Meta:
        verbose_name = "Перелет"
        verbose_name_plural = "Перелеты"

    def __str__(self):
        return '<Leg id={} code={}>'.format(self.id, self.code)


class Segment(models.Model, DateTimeMixin):
    """
    Сегмент из нескольких полетов, входит в Variant

    В полях содержатся агрегированные данные из перелетов, входящих в этот сегмент.
    Поля nullable, потому что сначала сохраняется пустой сегмент, а уже потом, когда
    есть id, к нему привязываются ноги. Иначе джанга не даст сделать `legs.set([])`
    """

    code = models.CharField('Код сегмента', max_length=50, null=True, unique=True)

    departure = models.CharField('Аэропорт вылета', max_length=3, null=True)
    departure_date = models.DateField('Дата отправления', null=True)
    departure_time = models.TimeField('Время отправления', null=True)

    arrival = models.CharField('Аэропорт прибытия', max_length=3, null=True)
    arrival_date = models.DateField('Дата прибытия', null=True)
    arrival_time = models.TimeField('Время прибытия', null=True)

    total_seconds = models.IntegerField('Время в пути (сек)', null=True)

    legs = models.ManyToManyField(Leg)

    class Meta:
        verbose_name = "Сегмент"
        verbose_name_plural = "Сегменты"


class Variant(models.Model, DateTimeMixin):
    """
    Вариант - группа из сегментов

    Перелеты в варианте могут быть разрознены по дням. Чаще всего это путешествие
    из сегментов Туда и Обратно.
    """
    code = models.CharField('Код варианта', max_length=50, null=True, unique=True)

    departure = models.CharField('Аэропорт вылета', max_length=3, null=True)
    departure_date = models.DateField('Дата отправления', null=True)
    departure_time = models.TimeField('Время отправления', null=True)

    arrival = models.CharField('Аэропорт прибытия', max_length=3, null=True)
    arrival_date = models.DateField('Дата прибытия', null=True)
    arrival_time = models.TimeField('Время прибытия', null=True)

    total_seconds = models.IntegerField('Время в пути (сек)', null=True)

    segments = models.ManyToManyField(Segment)

    class Meta:
        verbose_name = "Вариант перелета"
        verbose_name_plural = "Варианты перелетов"
