
class Serializer:

    def serialize_variants(self, variants):
        """
        Преобразовывает модели в plain-объекты, пригодные для вывода в JSON
        """

        # обнуляемся
        self.flight_legs = []
        self.leg_code_index = {}  # code => number like 0,3,4

        tickets = []

        for variant in variants:

            variant_dict = {}
            variant_dict['segments'] = []
            variant_dict['price'] = {}
            variant_dict['url'] = ''
            variant_dict['terms'] = {}

            self._fill_segment(variant, variant_dict)

            tickets.append(variant_dict)

        return tickets, self.flight_legs


    def _fill_segment(self, variant, variant_dict):

        for segment in variant.segments.all():
            segment_dict = {}
            segment_dict['flights'] = []

            self._fill_flights(segment, segment_dict)

            variant_dict['segments'].append(segment_dict)

    def _fill_flights(self, segment, segment_dict):

        for leg in segment.legs.all():

            flight_index = self._push_flight_leg(leg)
            segment_dict['flights'].append(flight_index)

        # variant_dict['segments'].append(segment_dict)

        # print(variant_dict, flight_legs)
        # tickets.append(variant_dict)

    def _push_flight_leg(self, leg):
        """
        Добавляет полет в массив self.flight_legs и возвращает его числовой индекс
        Если лег уже есть, то повторно не добавляет
        """

        if leg.code not in self.leg_code_index:

            # сам перелет кладем в flight_legs, запоминая его последовательный
            # номер, начиная с нуля
            numeric_index = len(self.flight_legs)
            self.leg_code_index[leg.code] = numeric_index
            self.flight_legs.append(self.serialize_leg(leg))

        return self.leg_code_index[leg.code]

    def serialize_leg(self, leg):
        leg_dict = {}
        leg_dict['origin'] = leg.departure
        leg_dict['destination'] = leg.arrival
        leg_dict['arrival_date_time'] = '{:%Y-%m-%d %H:%M}'.format(leg.arrival_datetime)
        leg_dict['departure_date_time'] = '{:%Y-%m-%d %H:%M}'.format(leg.departure_datetime)
        leg_dict['carrier_designator'] = {}

        return leg_dict
