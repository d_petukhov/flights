from datetime import date, time

from lxml import etree

from .importer import Deserializer, Syncronizer


class TestDeserializer:
    def test(self):
        """
        Десериалайзер работает в целом
        """

        xml = xml_variant.format(xml_flight)

        tree = etree.fromstring(xml)
        data = Deserializer.deserialize_variant(tree)

        assert data['price'] == 8366
        assert data['currency'] == 'RUB'
        assert data['isCharter'] == False
        assert data['segment'][0]['flight'][0]['departureDate'] == date(2020,4,20)
        assert data['segment'][0]['flight'][0]['departureTime'] == time(9, 55)
        assert data['segment'][0]['flight'][0]['cabin'] == True

    def test_prepare_segments(self):
        # сегмент превращается в словарь
        variant = {'segment': {}}
        Deserializer.prepare_segments(variant)
        assert isinstance(variant['segment'], list)

    def test_prepare_segments_already_list(self):
        variant = {'segment': [{}, {}]}
        Deserializer.prepare_segments(variant)
        assert isinstance(variant['segment'], list)

    def test_prepare_variant(self):
        data = {'price': '8366', 'currency': 'RUB', 'isCharter': 'false'}
        Deserializer.prepare_variant(data)
        # типы конвертируются
        assert data['price'] == 8366
        assert data['currency'] == 'RUB'
        assert data['isCharter'] == False

xml_variant = '''
<variant>
    <price>8366</price>
    <currency>RUB</currency>
    <isCharter>false</isCharter>
    <segment>
        {}
    </segment>
</variant>
'''

xml_flight = '''
    <flight>
        <operatingCarrier>S7</operatingCarrier>
        <marketingCarrier>S7</marketingCarrier>
        <number>1006</number>
        <departure>LED</departure>
        <departureDate>2020-04-20</departureDate>
        <departureTime>09:55</departureTime>
        <arrival>DME</arrival>
        <arrivalDate>2020-04-20</arrivalDate>
        <arrivalTime>11:25</arrivalTime>
        <equipment>319</equipment>
        <cabin>Y</cabin>
        <baggage>0PC</baggage>
        <fareCode>WBSOW</fareCode>
    </flight>
'''
