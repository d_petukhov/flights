import json

from django.shortcuts import render
from django.http import HttpResponse

from .models import Variant, Segment, Leg
from .serializers import Serializer

def all(request):
    # самый быстрый перелет (Variant)
    var = Variant.objects.order_by('-total_seconds').first()
    print(var)

    # сериализуем в объект, который можно показать наружу
    s = Serializer()
    tickets, legs = s.serialize_variants([var])

    response = {
        'result': {
            'tickets': tickets,
            'flight_legs': legs,
        }
    }

    return HttpResponse('<pre>' + json.dumps(response, indent=4) + '</pre>')


def fast(request):
    pass


def cheap(request):
    pass

def optimal(request):
    pass
